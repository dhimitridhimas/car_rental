import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { BackendService } from '../backend.service';
import { Branch } from '../models';

@Component({
  selector: 'app-branch',
  templateUrl: './branch.component.html',
  styleUrls: ['./branch.component.css']
})
export class BranchComponent implements OnInit {

  loading = false;
  branches: Branch[] = [];

  constructor(private backendService: BackendService,
    private router:Router) { }

    ngOnInit(): void {
      this.backendService.getBranch().subscribe((result: Branch[]) => {
        console.log(result)
        this.branches = result;
  
      })
    }
    delete(branch:Branch):void{
      this.backendService.deleteBranch(branch.id).subscribe(()=>{
        this.branches = this.branches.filter(b => b.id !== branch.id)
      }
      )
    }
    addItem(): void{
      this.router.navigate(['add-branch']);
    }
  
  }