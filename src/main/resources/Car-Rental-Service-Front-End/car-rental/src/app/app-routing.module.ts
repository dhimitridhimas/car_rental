import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AppComponent } from './app.component';
import { RouterModule, Routes } from '@angular/router';
import { CarComponent } from './car/car.component';
import { CarFormComponent } from './car-form/car-form.component';
import { HomeComponent } from './home/home.component';
import { BranchComponent } from './branch/branch.component';
import { AboutUsComponent } from './about-us/about-us.component';

const routes: Routes = [
  {path: 'cars', component: CarComponent},
  {path:'edit-car/:id',component:CarFormComponent},
  {path:'add-car',component:CarFormComponent},
  {path:'home',component:HomeComponent},
  {path:'branch',component:BranchComponent},
  {path:'about-us',component:AboutUsComponent}
];
@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
