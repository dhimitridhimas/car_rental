export interface Car {
    id: number;
    brand: string;
        model: string;
        bodyType: string;
        year: number;
        colour: string;
        amount: number;
        mileage: number;
        branchId:number;
} 
export interface Branch{
    id:number;
    address:string;
    price:number;
    rentalId:number;
}
export class SaveClass {
    address:string;
    branchId: number;
}