import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Router } from '@angular/router';
import { Car } from '../models';

@Component({
  selector: 'app-car-item',
  templateUrl: './car-item.component.html',
  styleUrls: ['./car-item.component.css']
})
export class CarItemComponent implements OnInit {

  @Input() car: Car | undefined;
  @Output() deletedCar = new EventEmitter<Car>(undefined);
  constructor(private router: Router) { }
  ngOnInit() {
  }
  
  editCarItem() {
   this.router.navigate([`edit-car/${this.car.id}`]);
  }
  deleteCarItem() {
    // i dergojme parent component user object qe duhet te fshije
    this.deletedCar.emit(this.car);
  }
  


}
