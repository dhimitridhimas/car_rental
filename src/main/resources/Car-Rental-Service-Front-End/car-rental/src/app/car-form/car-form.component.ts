import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { BackendService } from '../backend.service';
import { BranchComponent } from '../branch/branch.component';
import { Branch, Car, SaveClass } from '../models';

@Component({
  selector: 'app-car-form',
  templateUrl: './car-form.component.html',
  styleUrls: ['./car-form.component.css']
})
export class CarFormComponent implements OnInit {
  car:Car;
  branchId:number;
  branch:Branch;
  class: SaveClass = new SaveClass();
  selectedBranch;

  branches: Branch[] = []

  constructor(
    private route: ActivatedRoute,
    private backendService:BackendService,
    private router:Router
  ) { }

  ngOnInit(): void {
    this.getCar();
    this.getBranches()
  }

  getBranches(): void{
    
    
    this.backendService.getBranch().subscribe(branches => this.branches=branches);
  }
  
  getCar(): void{
    const id = +this.route.snapshot.paramMap.get('id');
    this.branchId= +this.route.snapshot.paramMap.get('id');
    if(id){
      this.backendService.getCar(id).subscribe(car => this.car=car);
    }else{
      this.car={
        id: null,
        brand: null,
        model: null,
        bodyType: null,
        year: null,
        colour: null,
        mileage: null,
        amount: null,
        branchId: null
      }
    }
  }
  goBack(): void {
    this.router.navigate(['cars']);
  }
  save():void{
    console.log(this.selectedBranch)
    this.car.branchId = this.selectedBranch;
    if(this.car.id){
      console.log(this.branchId);
    this.class.branchId = this.branchId;
      this.backendService.updateCar(this.car)
      .subscribe(() =>this.goBack());
    }
    else{
      console.log(this.branchId);
      this.class.branchId = this.branchId;
      this.backendService.addCar(this.car).subscribe(() =>{
        console.log("Car added");
        this.goBack;
      });
    }
  }
  

}
