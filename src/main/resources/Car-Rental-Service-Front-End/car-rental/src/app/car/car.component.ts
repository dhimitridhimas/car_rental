import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { BackendService } from '../backend.service';
import { Car } from '../models';

@Component({
  selector: 'app-car',
  templateUrl: './car.component.html',
  styleUrls: ['./car.component.css']
})
export class CarComponent implements OnInit {

loading = false;
cars: Car[] = [];

  constructor(private backendService: BackendService,
    private router:Router) { }
    myimage: string = "assets/images/HomePicture.jpg"

  ngOnInit(): void {
    this.backendService.getCars().subscribe((result: Car[]) => {
      console.log(result)
      this.cars = result;

    })
  }
  delete(car:Car):void{
    this.backendService.deleteCar(car.id).subscribe(()=>{
      this.cars = this.cars.filter(c => c.id !== car.id)
    }
    )
  }
  addItem(): void{
    this.router.navigate(['add-car']);
  }

}
