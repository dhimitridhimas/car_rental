import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Branch, Car } from './models';

@Injectable({
  providedIn: 'root'
})
export class BackendService {
 

  private backendUrl = 'http://localhost:8080';
  httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' }),
  };
  constructor(private http: HttpClient) { }

  getCars(): Observable<Car[]> {
    // marrim gjithe users array, nuk ka nevoje per parametra shtese ne url
    return this.http.get<Car[]>(`${this.backendUrl}/car`);
  }

  updateCar(car:Car) : Observable<Car> {
    const url = `${this.backendUrl}/car/${car.id}`;
    return this.http.put<Car>(url,car,this.httpOptions);
  }
  addCar(car: Car) : Observable<Car>{
    const url = `${this.backendUrl}/car`;
    return this.http.post<Car>(url,car,this.httpOptions);
  }
  getBranch():Observable<Branch[]>{
    const url = `${this.backendUrl}/branch`;
    return this.http.get<Branch[]>(url);
  }
  getCar(carId:number):Observable<Car>{
    const url = `${this.backendUrl}/car/${carId}`;
    return this.http.get<Car>(url);
  }
  deleteCar(carId:number):Observable<void>{
    const url = `${this.backendUrl}/car/${carId}`
    return this.http.delete<void>(url);

  }
  deleteBranch(branchId:number):Observable<void>{
    const url = `${this.backendUrl}/branch/${branchId}`
    return this.http.delete<void>(url);

  }
}
