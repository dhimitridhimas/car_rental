import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { BackendService } from '../backend.service';
import { Branch } from '../models';

@Component({
  selector: 'app-branch-form',
  templateUrl: './branch-form.component.html',
  styleUrls: ['./branch-form.component.css']
})
export class BranchFormComponent implements OnInit {
  

  constructor(
    private route: ActivatedRoute,
    private backendService:BackendService,
    private router:Router
  ) { }

  ngOnInit(): void {

  }

}
