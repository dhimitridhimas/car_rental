package com.sda.carrental.model.dto;

import lombok.*;

@Builder
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class UserDto {
    private Long id;
    private  String email;
    private String address;
    private String firstName;
    private String lastName;
    //create a class for roles for parameters such as id,names in role as well as description
    // create a new repository roles and as well as a controller roles for
    // To create roles
    // create Crud operations for it such as delete,put etc
    //modify user entity and add many to one role on the object

}
