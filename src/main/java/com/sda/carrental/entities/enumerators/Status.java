package com.sda.carrental.entities.enumerators;

public enum Status {
    BOOKED,AVAILABLE,UNAVAILABLE
}
